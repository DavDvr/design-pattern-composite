<?php

namespace Composite\App;

use AbstractComponent;

class File extends AbstractComponent
{

    /**
     * File constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        parent::__construct($name);
    }

    public function display(): void
    {
        $tab = $this->indentation();
        echo $tab. "File: ". $this->name. " <br>";
    }
}