<?php


namespace Composite\App;

use AbstractComponent;
require_once ("AbstractComponent.php");

class FolderComposite extends \AbstractComponent
{

    /**
     * @var array<AbstractComponent>
     */
    private array $components = [];
    /**
     * FolderComposite constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        parent::__construct($name);
    }

    public function display(): void
    {
        $tab = $this->indentation();
        echo $tab . "Folder: ". $this->name. "<br>";

        foreach ($this->components as $component){
           $component->display();
        }
    }

    /**
     * @param AbstractComponent $abstractComponent
     * I define the level of component + 1
     * I add component in my array components
     * @return AbstractComponent
     */
    public function addComponent(AbstractComponent $abstractComponent): AbstractComponent
    {
        $abstractComponent->level = $this->level + 1;
        $abstractComponent->parent = $this;
        $this->components[] = $abstractComponent;
        return $abstractComponent;
    }

    /**
     * @param string $name
     * Function allow search a component with her name
     * @return string
     */
    public function getComponent(string $name): string
    {
        foreach($this->components as $component) {
            if ($component->name === $name) {
                return $component->name;
            } else {
                return false;
            }
        }
    }

    /**
     * Remove component
     * @param AbstractComponent $abstractComponent
     */
    public function remove(AbstractComponent $abstractComponent){
        if (in_array($abstractComponent, $this->components)){
            unset($this->components[array_search($abstractComponent,  $this->components)]);
        }
    }

}