<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home composite</title>
</head>
<body>
<?php

use Composite\App\File;
use Composite\App\FolderComposite;
require_once (dirname(__DIR__)."/vendor/autoload.php");


    $root = new FolderComposite("Root");
    $folder1 = new FolderComposite("Structure");
    $folder2 = new FolderComposite("Comportement");
    $folder3 = new FolderComposite("Création");


    // i add folder to root
    $root->addComponent($folder1);
    $root->addComponent($folder2);
    $root->addComponent($folder3);

    // i create new file to each category of folder
    $folder1->addComponent(new File("Composite"));
    $folder1->addComponent(new File("Decorator"));

    $folder2->addComponent(new File("Strategy"));

    $folder3->addComponent(new File("Singleton"));
    $folder3->addComponent(new File("Abstract Factory"));

    // i create a new folder Structure1 with her file
      $folderStr1 =  $folder1->addComponent(new FolderComposite("Structure1"));


     //i create the file to Structure1
     $folderStr1->addComponent(new File("Composite version 2"));

     //i remove a component
     //$root->remove($folder1);

     //i search component
      //var_dump($folder2->getComponent("Strategy"));

    // i display the result
    $root->display();
?>
</body>
</html>
