<?php

use Composite\App\FolderComposite;

abstract class AbstractComponent
{

    /**
     * @var AbstractComponent
     */
    protected AbstractComponent $parent;

    /**
     * @var string
     * Name of component
     */
    protected string $name;

    /**
     * @var int
     * level of the element in hierarchy
     */
    protected int $level = 0;

    /**
     * AbstractComponent constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public abstract function display(): void;

    public function indentation(): string
    {

        $str = "";
        for ($i = 0; $i < $this->level; $i++){
            $str.=str_pad($str,6,"-",STR_PAD_RIGHT);
            $str = str_replace("-", "&nbsp;", $str);
        }
        return $str;
    }
}